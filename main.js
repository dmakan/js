let nb = [];
rand_nb = 10;

/* 
    cette fonction list_rand, prend un arg liste vide : return la liste vide avec 10 items selectionner 
    aléatoire entre 1 et 100
*/
function list_rand(list_nb) {
    let number = 10;
    while (number > list_nb.length){
        list_nb.push(Math.floor(Math.random() * 100));
    }

    return list_nb;
}

let list = list_rand([]);
console.log(" La liste aléatoire :",list,"\n");

/*
cette fonction nb_pair, prend un arg 0 entier : return la somme de nombre pair dans liste
*/
function nb_pair(pair) {
    for (let i = 0; i < 10; i++){
        let items = list[i];
        if (items % 2 === 0){
            pair ++;
        }
    }

    return pair;
}

let pair = nb_pair(0);
console.log("Les nombre pairs dans la liste est : " + pair);

/* 
cette fonction nb_impair, prend un arg 0 entier : return la somme de nombre impair dans liste
*/
function nb_impair(impair) {
    for (let i = 0; i < 10; i++){
        let items = list[i];
        if (items % 2 !== 0){
            impair ++;
        }
    }

    return impair;
}

let impair = nb_impair(0);
console.log("Les nombre impairs dans la liste est : " + impair);

/*
cette fonction comparaison prend 2 args entier : return le resultat de la comparaison entre le 2 arg
*/
function comparaison(pair, impair) {
    if (pair > impair){
        console.log("il y'a plus de nombre pairs que impairs");
    }
    else if (pair === impair){
        console.log("le nombre pairs est égal à la nombre impars");
    }
    else{
        console.log("il y'a plus de nombre impairs que pairs");
    }
    console.log(".....................................................");
}

let comparer = comparaison(pair, impair);

/* 
cette multiple5 prend un arg 0 entier : return le nombre multiple de 5 dans la liste
*/
function multiple5(mlt5) {
    for (let i = 0; i < 10; i++){
        let items = list[i];
        if (items % 5 === 0){
            mlt5 ++;
        }
    }

    return mlt5;
}

let result5 = multiple5(0);
console.log("Les nombre multiple de Cinq est : " + result5+'\n');